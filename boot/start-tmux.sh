#!/bin/bash

. ~/.bashrc

session="flatcat"

socketname=${HOME}/tmux-sock

tmux_cmd="tmux -S ${socketname}"

${tmux_cmd} start-server

# ${tmux_cmd} set-option -g default-shell /bin/sh

${tmux_cmd} has-session -t $session
if [ $? != 0 ]
then
  ${tmux_cmd} new-session -d -s $session -n ux
  sleep 5
  ${tmux_cmd} send-keys "cd $HOME/jetpack/flatcat_ux0" C-m
  ${tmux_cmd} send-keys "./ux0_serial -l" C-m
fi

# ${tmux_cmd} new-window -t $session:1 -n api
# ${tmux_cmd} send-keys "cd $HOME/jetpack/flatcat_app/api" C-m
# ${tmux_cmd} send-keys "export PYTHONPATH=$HOME/jetpack/flatcat_app:$PYTHONPATH" C-m
# ${tmux_cmd} send-keys ". $HOME/work/venv/bin/activate" C-m
# ${tmux_cmd} send-keys "gunicorn --reload api:app" C-m

${tmux_cmd} new-window -t $session:1 -n flatcatapp
${tmux_cmd} send-keys "tail -f /var/log/gunicorn/*.log" C-m

# gunicorn is now handled via systemd
# sudo systemctl restart flatcatapp

# ${tmux_cmd} attach-session -t $session
