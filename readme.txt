
+------------------+
| AUTOSTART HOW-TO |
+------------------+

1. Install tmux

	sudo apt install tmux1

2. create tmux.conf in homefolder containing this line
   or copy the file from the bootscripts folder
	
	set-option -g default-shell /bin/bash

3. setup crontab

	crontab crontabfile

4. reboot and test
	
	sudo reboot
	...
	tmux attach-session -t flatcat
	(can also be attached to the end of ~/.bashrc)


5. setup Access Point


   a.) sudo apt-get install dnsmasq hostapd


   b.) add to the end of /etc/dnsmasq.conf

     interface=lo,ap0
     no-dhcp-interface=lo,wlan0
     bind-interfaces
     server=8.8.8.8
     domain-needed
     bogus-priv
     dhcp-range=192.168.10.50,192.168.10.150,12h


   c.) edit /etc/default/hostapd

     DAEMON_CONF="/etc/hostapd/hostapd.conf"


   d.) add start-ap-managed_wifi.sh to cron 
   //TODO full tutorial, make simpler
   https://blog.thewalr.us/2017/09/26/raspberry-pi-zero-w-simultaneous-ap-and-managed-mode-wifi/



 UART-Settings

 remove "console=serial0,115200" from /boot/cmdline.txt

  add to /boot/config.txt
  "
   # UART/RS485
   dtoverlay=miniuart-bt
   enable_uart=1

   # Disable HDMI
   hdmi_blanking=2
  "


$ echo 'dtoverlay=pi3-disable-bt' | sudo tee -a /boot/config.txt
$ sudo systemctl disable hciuart
Removed /etc/systemd/system/multi-user.target.wants/hciuart.service
$ sudo reboot




sudo systemctl disable apt-daily.service
saves 10 sec on boot time


add to /boot/config.txt

# Disable Bluetooth
dtoverlay=disable-bt


Disable related services Permalink

sudo systemctl disable hciuart.service
sudo systemctl disable bluetooth.service

sudo systemctl disable triggerhappy.service
sudo systemctl disable keyboard-service
