"""bootstrap

bootstrap a flatcat system filesystem with required unique
configuration, name, and MAC address.
"""
import argparse, sys, os, json

from string import Template

def bootstrap_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mac-address', type=str, default=None, help='MAC address of Pi')
    parser.add_argument('-n', '--hostname', type=str, default='flatcat', help='hostname of Pi')
    parser.add_argument('-r', '--rootfs', type=str, default='./', help='Path to rootfs')
    return parser

def main(args):
    conffiles = [
        '/etc/hosts',
        '/etc/hostname',
        '/etc/hostapd/hostapd.conf',
        '/home/pi/jetpack/flatcat_app/config_flatcat.json',
        '/etc/network/interfaces'
    ]
    for conffile in conffiles:
        # open ./path as template from flatcat_setup
        # insert variables from arguments
        # write to file in rootfs/path
        with open(f'.{conffile}.template') as f:
            tmpl = Template(f.read())
            conftext = tmpl.substitute({
                'hostname': args.hostname,
                'mac_address': args.mac_address,
                'wpa_passphrase': 'fl4tc4t*',
            })
            # print(conftext.strip())
            conffile_out = f'{args.rootfs}/{conffile}'
            print(f'opening {conffile_out} for write')
            try:
                g = open(conffile_out, 'w')
                g.write(conftext) # .strip()
                g.close()
            except PermissionError as e:
                print(f'failed to open {conffile_out} for write, try running with sudo')
                
if __name__ == '__main__':
    parser = bootstrap_parser()
    args = parser.parse_args()

    main(args)
